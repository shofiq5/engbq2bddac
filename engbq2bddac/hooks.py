# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "engbq2bddac"
app_title = "Air Galaxy Maldivian "
app_publisher = "Shofiq"
app_description = "Air Galaxy Maldivian Customization."
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "shofiq5@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/engbq2bddac/css/engbq2bddac.css"
# app_include_js = "/assets/engbq2bddac/js/engbq2bddac.js"

# include js, css files in header of web template
# web_include_css = "/assets/engbq2bddac/css/engbq2bddac.css"
# web_include_js = "/assets/engbq2bddac/js/engbq2bddac.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "engbq2bddac.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "engbq2bddac.install.before_install"
# after_install = "engbq2bddac.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "engbq2bddac.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"engbq2bddac.tasks.all"
# 	],
# 	"daily": [
# 		"engbq2bddac.tasks.daily"
# 	],
# 	"hourly": [
# 		"engbq2bddac.tasks.hourly"
# 	],
# 	"weekly": [
# 		"engbq2bddac.tasks.weekly"
# 	]
# 	"monthly": [
# 		"engbq2bddac.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "engbq2bddac.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "engbq2bddac.event.get_events"
# }
fixtures = ["Report", "Role Profile", "Role", "Custom Field", "Custom Script", "Property Setter", "Workflow", "Workflow State", "Workflow Action"]
